#include <stdio.h>

void pattern(int n); //function to control the pattern
void Row(int m); //function to print each line of the pattern
int NoOfRows=1; //initial value for number of rows
int t; //variable to get the input

void pattern(int n)
{

    if(n>0)
    {
        Row(NoOfRows);
        printf("\n");
        NoOfRows++;
        pattern(n-1);

    }
}


void Row(int m)
{
    if(m>0)
    {
        printf("%d",m);
        Row(m-1);
    }
}

int main()
{

    printf("Enter the No. of Rows : ");
    scanf("%d",&t);
    pattern(t);
    return 0;
}
